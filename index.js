import { Inflate, inflateSync } from 'fflate'


// AsyncInflate was also tested but found to be slower and buggy (would
// sometimes throw errors about a detached ArrayBuffer). Could possibly be
// revisted in the future as fflate matures.
class FflateSink {
  constructor(uncompressedSize) {
    this.outputBuffer = new Uint8Array(uncompressedSize)
    this.bufferPromise = new Promise( (resolve) => this.bufferResolver = resolve)
    let bytesWritten = 0
    this.fflate = new Inflate((data, final) => {
      this.outputBuffer.set(data, bytesWritten)
      bytesWritten += data.byteLength
      if (final) {
        this.bufferResolver(this.outputBuffer)
      }
    })
  }

  write(chunk) {
    this.fflate.push(chunk)
  }

  close() {
    this.fflate.push([], true)
  }
}

class FflateTransformer {
  constructor() {
    this.fflate = new Inflate((data) => {
      this.controller.enqueue(data)
    })
  }

  start(controller) {
    this.controller = controller
  }

  transform(chunk) {
    this.fflate.push(chunk)
  }

  flush() {
    this.fflate.push([], true)
  }
}


/** Setup function for decoder. No setup is needed, simply returns the object */
export function fflateDecoderSetup() {
  return Promise.resolve({
    createSink: (uncompressedSize) => {
      const decoder = new FflateSink(uncompressedSize)
      return [decoder, decoder.bufferPromise]
    },
    createTransformer: () => new FflateTransformer(),
    decodeBuffer: async (buffer, uncompressedSize) =>
      // inflateSync() benchmarked to be ~3x faster than inflate() on Chromium,
      // on Firefox inflate is slightly faster. Benched with consume: true and
      // size options set. Chromium 87, Firefox 85, fflate 0.4.4
      inflateSync(buffer, new Uint8Array(uncompressedSize))
  })
}
