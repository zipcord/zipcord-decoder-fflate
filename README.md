# zipcord-decoder-fflate

Decoder for [Zipcord](https://gitlab.com/zipcord/zipcord) using
[fflate](https://github.com/101arrowz/fflate) as its backend. You will probably
not need to use this directly, as Zipcord will use this decoder by default if
you do not set `decoderSetup`.

## Usage

Install with `npm install zipcord-decoder-fflate` or the equivalant for your
package manager. Use as follows:

```typescript
import { Zipcord } from 'zipcord';
import { fflateDecoderSetup } from 'zipcord-decoder-fflate';
// Setting decoderSetup is optional as Zipcord will use fflateDecoderSetup by
// default if you do not set the option
const options = {
    decoderSetup: fflateDecoderSetup
};
const archive = await Zipcord.open('https://example.com/archive.zip', options);
// Extract files as normal, now decompressing with fflate
```

## License

zipcord-decoder-fflate is copyright © David Powell and is licenced under MIT. See
LICENSE.txt for the full text of the license.

[fflate](https://github.com/101arrowz/fflate) is copyright © 2020 Arjun Barrett
and licenced under MIT.
