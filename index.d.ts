/** Setup function for decoder, to be given directly to `decoderSetup` */
export declare function fflateDecoderSetup(): Promise<{
  createSink: (uncompressedSize: number) => [sink: UnderlyingSink<Uint8Array>, promise: Promise<Uint8Array>],
  createTransformer: (uncompressedSize: number) => Transformer<Uint8Array, Uint8Array>,
  decodeBuffer: (inputBuffer: Uint8Array, uncompressedSize: number) => Promise<Uint8Array>,
}>
