module.exports = {
  extends: [
    'eslint:recommended',
  ],
  env: {
    browser: true,
    es2020: true
  },
  parserOptions: {
    sourceType: "module"
  },
  rules: {
    'eqeqeq': 'error',
    'key-spacing': 'error',
    // Allow console.log for debugging because I am lazy and bad at debuggers
    'no-console': 'warn',
    'no-multiple-empty-lines': 'error',
    'no-trailing-spaces': 'error',
    'no-unneeded-ternary': 'error',
    'prefer-const': 'warn',
    'semi': [
      'error',
      'never',
      {
        beforeStatementContinuationChars: 'always'
      }
    ],
  }
}
