import { fflateDecoderSetup } from './index.js'


const SAMPLE_ARRAY = [0x55,0xc8,0xb1,0x0d,0x80,0x30,0x0c,0x04,0xc0,0x9e,0x29,0x7e,0x02,0x06,0x60,0x0e,0x16,0x80,0xf8,0x43,0x2c,0x25,0x36,0x8a,0x2d,0x65,0x7d,0x6a,0xae,0xbc,0xb3,0x69,0xe0,0x42,0x77,0x77,0x7b,0x50,0xb5,0x13,0x07,0x16,0x61,0xa4,0x20,0x1d,0x41,0x22,0x1b,0x21,0x5a,0x2b,0x27,0xad,0x10,0x37,0x73,0x91,0xf6,0xeb,0x44,0xf1,0xf1,0x4e,0x46,0xa8,0x1b,0x06,0xb3,0xb9,0xc4,0xbe,0x7d]
const SAMPLE = new Uint8Array(SAMPLE_ARRAY)
const SAMPLE_CHUNK1 = SAMPLE.slice(0, 40)
const SAMPLE_CHUNK2 = SAMPLE.slice(40)
const SAMPLE_UNCOMPRESSED_ARRAY = [0x54,0x68,0x69,0x73,0x20,0x61,0x20,0x6c,0x6f,0x6f,0x6f,0x6e,0x67,0x20,0x66,0x69,0x6c,0x65,0x20,0x3a,0x20,0x77,0x65,0x20,0x6e,0x65,0x65,0x64,0x20,0x74,0x6f,0x20,0x73,0x65,0x65,0x20,0x74,0x68,0x65,0x20,0x64,0x69,0x66,0x66,0x65,0x72,0x65,0x6e,0x63,0x65,0x20,0x62,0x65,0x74,0x77,0x65,0x65,0x6e,0x20,0x74,0x68,0x65,0x20,0x64,0x69,0x66,0x66,0x65,0x72,0x65,0x6e,0x74,0x20,0x63,0x6f,0x6d,0x70,0x72,0x65,0x73,0x73,0x69,0x6f,0x6e,0x20,0x6d,0x65,0x74,0x68,0x6f,0x64,0x73,0x2e,0x0a]
const SAMPLE_UNCOMPRESSED = new Uint8Array(SAMPLE_UNCOMPRESSED_ARRAY)
const SAMPLE_UNCOMPRESSED_SIZE = 94

const WRITE_CONTROLLER = {
  error: (e) => {throw new Error('Sink error: ' + e)}
}

/** Merge array of Uint8Arrays into a single buffer */
function mergeBuffers(buffers) {
  const outSize = buffers.reduce( (acc, cur) => acc + cur.byteLength, 0)
  const out = new Uint8Array(outSize)
  let bytesMerged = 0
  for (const buffer of buffers) {
    out.set(buffer, bytesMerged)
    bytesMerged += buffer.byteLength
  }
  return out
}

/** Compare a buffer against SAMPLE_UNCOMPRESSED and throw if different */
function testCompare(testBuffer, message) {
  if (Buffer.compare(testBuffer, SAMPLE_UNCOMPRESSED) !== 0)
    throw new Error('Output does not match expected: ' + message)
}

const decoder = await fflateDecoderSetup()

// decodeBuffer
const buffer = await decoder.decodeBuffer(SAMPLE, SAMPLE_UNCOMPRESSED_SIZE)
testCompare(buffer, 'decodeBuffer')

// createSink
const [sink, resultSink] = decoder.createSink(SAMPLE_UNCOMPRESSED_SIZE)
sink.write(SAMPLE, WRITE_CONTROLLER)
sink.close()
testCompare(await resultSink, 'createSink')

const [sinkChunked, resultChunked] = decoder.createSink(SAMPLE_UNCOMPRESSED_SIZE)
sinkChunked.write(SAMPLE_CHUNK1, WRITE_CONTROLLER)
sinkChunked.write(SAMPLE_CHUNK2, WRITE_CONTROLLER)
sinkChunked.close()
testCompare(await resultChunked, 'createSink (multiple chunks)')

// createTransformer
const outChunks = []
const transformController = {
  enqueue: (chunk) => outChunks.push(chunk),
  // The following are unused but required
  desiredSize: 0,
  error: () => {},
  terminate: ()=> {}
}
const transformer = decoder.createTransformer(SAMPLE_UNCOMPRESSED_SIZE)
transformer.start(transformController)
transformer.transform(SAMPLE_CHUNK1, transformController)
transformer.transform(SAMPLE_CHUNK2, transformController)
transformer.flush(transformController)
testCompare(mergeBuffers(outChunks), 'createTransformer')

// Corrupt input data should throw
const [sinkCorrupt] = decoder.createSink(SAMPLE_UNCOMPRESSED_SIZE)
let thrown = false
try {
  sinkCorrupt.write(SAMPLE_CHUNK2, WRITE_CONTROLLER)
} catch {
  thrown = true
}
if (!thrown) {
  throw new Error('Corrupt input data did not throw error in decoder')
}

console.log('All tests completed successfully')
